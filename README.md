# Safari Extensions

A set of three, now defunct, extensions written for the browser **Safari**. The first two, *Dark Design* & *Darkness*, connect to websites I made largely focussed on collecting images. Once logged in, the extensions allow one to send images from the current page to your account.

![dark design login](image.jpg)

The last extension, *Private Mode*, was a very simple extension that provided a visual reminder that a window was *private*. Unlike other browsers, **Safari** doesn't render private windows differently.

These extensions are no longer maintained and sooner rather than later will not function with future versions of *Safari*.
