const $pop = safari.extension.popovers[0].contentWindow.document;
const $http = "http://captainlc.000space.com/darkdesign/";
const $popover = safari.extension.toolbarItems.filter(function (ti){
	return ti.popover == safari.self;
})[0];

function authenticate(){
	if(localStorage.dkdsusna || localStorage.dkdspswd){
		if(localStorage.dkdsstyle){
			if(localStorage.dkdsstyle == '0'){
				$pop.getElementsByTagName('body')[0].classList.add('darkness');
			}else{
				$pop.getElementsByTagName('body')[0].classList.remove('darkness');
			}
		}
		$pop.forms[0].parentElement.classList.add('hide');
		$pop.getElementById('links').classList.remove('blur');
		$pop.getElementById('sign_out').disabled = false;
		$pop.getElementsByClassName('progress-button')[0].classList.remove('loading');
	}else{
		$pop.getElementsByClassName('progress-button')[0].classList.remove('loading');
	}
}

function login(event){
	//login to site allowing extension to function
	//disable the default 
	if(event.preventDefault){
		event.preventDefault();
	}
  	event.returnValue = false;

  	var url = event.target.formElem.action;
  	var username = event.target.formElem.children[2].children[1];
  	var pass = event.target.formElem.children[3].children[1];
  	localStorage.dkdsusna = btoa(username.value);
  	localStorage.dkdspswd = btoa(pass.value);
  	if(username.length < 1 || pass.length < 1){
  		//display error svg class
  		var loading = $pop.getElementsByClassName('progress-button')[0];
  		loading.classList.remove('loading');
  		loading.classList.add('error');
  		event.target.formElem.getElementsByClassName('error_msg')[0].classList.remove('hide');

  	}else{
  		var params = {
  			'action' : url,
			'funct' : loginResponse,
			'form' : event.target.formElem,
			'type' : 'post',
			'data' : false
  		};
  		ajaxRequest(params);
  	}
}

function addLinks($links, $count){
	authenticate();

	if($links.indexOf('http') === 0){
		var $container = $pop.getElementById('links'),
		$instruction = $pop.getElementById('instructions'),
		$newLink = $pop.createElement('li');

		$newLink.classList.add('new_link');
		$newLink.innerHTML = "<a href='"+$links+"'><span class='big_num'>"+$count+"</span>Image</a><button title='Remove This Link' class='remove_link'></button>";
		$container.insertBefore($newLink);
		$instruction.classList.add('hide');

		if($pop.getElementById('login').classList[1] !== 'hide'){
			openPopover();
		}

		if(safari.extension.globalPage.contentWindow.queryLinkCount() < 1){
			enableDisableBtn(true);
		}else{
			enableDisableBtn(false);
		}
	}
}

function enableDisableBtn(disable){
	var test = $pop.getElementById('submit_links');
	var best = $pop.getElementById('clear_links');

	if(disable){
		test.disabled = true;
		best.disabled = true;
	}else{
		test.disabled = false;
		best.disabled = false;
	}
}

function bindSubmit(form, funct){
	form.addEventListener('submit', funct, false);
	form.formElem = form;
}

function submitLinks(){
	//set up ajax to send form data to dark design, wait for response, remove links are restore or alert user for errors

	var $link = $pop.getElementsByClassName('new_link'),
	$links_arr = [];

	for(var i = 0; i < $link.length; i++){
		$links_arr.push($link[i].children[0].href);
	}

	var $final_links = JSON.stringify($links_arr);
	var url = $http + "login/extLogin";
	var params = {
		'action' : url,
		'funct' : linkResponse,
		'form' : false,
		'type' : 'post',
		'data' : $final_links
	};

	ajaxRequest(params);
	showMsg(1);
}

function linkResponse(){
	var loading = this.loading,
	response = {'error' : true};

	try{
		response = JSON.parse(this.response);
	}catch(e){
		console.log('Response not valid JSON. ' + e);
	}

	if(response.error){
		//handle error
		loading.classList.remove('loading');
		loading.classList.add('error');

		setTimeout(function(){
			loading.classList.remove('error');
		}, 1000);

		$pop.getElementById('clear_links').click();
		hideMsg(1);
	}else{
		//success! - remove links, disable submit buttons, restore instructions
		loading.classList.remove('loading');
		loading.classList.add('success');
		localStorage.dkdsstyle = response.user_style;

		setTimeout(function(){
			loading.classList.remove('success');
		}, 1000);

		$pop.getElementById('clear_links').click();
		hideMsg(1);
	}
}

function ajaxRequest(params){
	//add loading class to loading svg
	var loading = $pop.getElementsByClassName('progress-button')[0];
	loading.classList.add('loading');

	var xhr = new XMLHttpRequest();
	xhr.loading = loading;
	xhr.onload = params.funct;
	xhr.onerror = ajaxError;
	//xhr.withCredentials = true;
	xhr.open(params.type, params.action, true);
	if(params.form){
		xhr.send(new FormData(params.form));
	}else if(params.data){
		var fData = new FormData();
		fData.append('urls', params.data);
		fData.append('user_name', atob(localStorage.dkdsusna));
		fData.append('user_password', atob(localStorage.dkdspswd));
		xhr.send(fData);
	}else{
		xhr.send();
	}	
}

function ajaxError(){
	var response = this.response;
	var loading = this.loading;
	console.log(respsonse);

	loading.classList.remove('loading');
	loading.classList.add('error');

	setTimeout(function(){
		loading.classList.remove('error');
	}, 1000);

	$pop.getElementById('clear_links').click();
	hideMsg(1);
}

function loginResponse(){
	var response = { 'login' : 0 };
	try{
		response = JSON.parse(this.response);
	}catch(e){
		console.log('Response not valid JSON');
	}

	var error_msg = $pop.getElementsByClassName('error_msg')[0];
	var loading = this.loading;
	if(response.login === 1){
		//success!
		localStorage.dkdsstyle = response.user_style;
		loading.classList.remove('loading');
		loading.classList.add('success');

		setTimeout(function(){
			loading.classList.remove('success');
		}, 1000);

		if(error_msg.classList.contains('hide')){/**good!*/}else{
			error_msg.classList.add('hide');
		}

		$pop.forms[0].reset();
		$pop.getElementById('login').classList.add('hide');
		$pop.getElementById('links').classList.remove('blur');
		$pop.getElementById('sign_out').disabled = false;
	}else{
		loading.classList.remove('loading');
		loading.classList.add('error');
		if(error_msg.classList.contains('hide')){error_msg.classList.remove('hide');}
		setTimeout(function(){
			loading.classList.remove('error');
		}, 1000);
		localStorage.removeItem("dkdsusna");
		localStorage.removeItem("dkdspswd");	
		localStorage.removeItem("dkdsstyle");
	}
}

function clearLinks(){
	//remove links with the class new_link
	var new_links = $pop.getElementsByClassName('new_link');
	for(var i = new_links.length; i !== 0; i--){
		new_links[i-1].classList.add('fadeOut');
	}
	setTimeout(function(){
		for(var i = new_links.length; i !== 0; i--){
			new_links[i -1].remove();
		}
	}, 700);

	$pop.getElementById('instructions').classList.remove('hide');
	enableDisableBtn(true);
	safari.extension.globalPage.contentWindow.resetLinkCount();
	safari.extension.globalPage.contentWindow.updateBadge();
}

function clearSingleLink(){
	//change $pop to $pop when using as extension
	$pop.getElementById('links').addEventListener('click', function(event){
		if(event.target.nodeName === 'BUTTON'){
			event.target.parentElement.classList.add('fadeOut');
			//safari.extension.globalPage.contentWindow.decrementLinkCount()
			setTimeout(function(){
				event.target.parentElement.remove();
				if($pop.getElementsByClassName('new_link').length < 1){
					$pop.getElementById('instructions').classList.remove('hide');
					enableDisableBtn(true);
				}
			}, 800);

			safari.extension.globalPage.contentWindow.decrementLinkCount();
			safari.extension.globalPage.contentWindow.updateBadge();
			if(safari.extension.globalPage.contentWindow.queryLinkCount() < 1){
				$pop.getElementById('instructions').classList.remove('hide');
			}
		}
	});
}

function signOut(){
	//send request to logout from site
	var params = {
		'action' : $http + "login/signOut",
		'funct' : function(){
					var response = { 'logout' : 0 };
					try{
						response = JSON.parse(this.response);
					}catch(e){
						//JSON parse will complain if the response isn't correct json
						console.log('Response not valid JSON');
					}
					if(response.logout === 1){
						$pop.forms[0].parentElement.classList.remove('hide');
						$pop.getElementById('links').classList.add('blur');
						$pop.getElementById('sign_out').disabled = true;
						enableDisableBtn(true);
						this.loading.classList.remove('loading');
						localStorage.removeItem("dkdsusna");
						localStorage.removeItem("dkdspswd");	
						localStorage.removeItem("dkdsstyle");
					}else{
						loading.classList.remove('loading');
						loading.classList.add('error');
						if(error_msg.classList.contains('hide')){error_msg.classList.remove('hide');}
						setTimeout(function(){
							loading.classList.remove('error');
						}, 1000);	
					}
				},
		'form' : false,
		'type' : 'get',
		'data' : false
	};

	ajaxRequest(params);
}

function footerBinding(){
	$pop.getElementById('footer').addEventListener('click', function(event){
		if(event.target.id === 'submit_links'){
			submitLinks();
		}else{
			clearLinks();
		}
	});
}

function openPopover(){
	$popover.showPopover();
}

function showMsg(msg_type){
	if($pop.getElementById('blanket').classList[0] === 'hide'){
		$pop.getElementById('blanket').classList.remove('hide');
	}
	if(msg_type === 1){
		if($pop.getElementById('wait').classList[0] === 'hide'){
			$pop.getElementById('wait').classList.remove('hide');
			$pop.getElementById('footer').classList.add('unselectable');
		}
	}else{
		if($pop.getElementById('limit').classList[0] === 'hide'){
			$pop.getElementById('limit').classList.remove('hide');
		}
	}
}

function hideMsg(msg_type){
	if($pop.getElementById('blanket').classList[0] !== 'hide'){
		$pop.getElementById('blanket').classList.add('hide');
	}
	if(msg_type === 1){
		if($pop.getElementById('wait').classList[0] !== 'hide'){
			$pop.getElementById('wait').classList.add('hide');
			$pop.getElementById('footer').classList.remove('unselectable');
		}
	}else{
		if($pop.getElementById('limit').classList[0] !== 'hide'){
			$pop.getElementById('limit').classList.add('hide');
		}
	}
}

function dismiss(){
	hideMsg(0);
}

$pop.addEventListener("DOMContentLoaded", function(){
	//on load functions
	//check if user is logged in 
	//display correct content depending on check
	clearSingleLink();
	footerBinding();
	authenticate();
	bindSubmit($pop.forms[0], login);
	$pop.getElementById('sign_out').addEventListener('click', signOut);
	$pop.getElementById('dismiss').addEventListener('click', dismiss);
});