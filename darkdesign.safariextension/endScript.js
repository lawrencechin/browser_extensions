//add event listener to check for right click/context menu events 
document.addEventListener("contextmenu", handleContextMenu, false);
//store data of clicks and used in global.html
function handleContextMenu(event){
	if(event.target.nodeName === 'IMG'){
		//check if image is a link e.g. thumbnails
		if(event.target.parentNode.nodeName === 'A'){
			//check if it links to a valid image
			var imgLinkTest = testParam(event.target.parentNode.href);
			var imgLink = loopObj(imgLinkTest) || 0;
			if(imgLink.length){
				safari.self.tab.setContextMenuEventUserInfo(event, imgLink);
				return;
			}else if(testLink(event.target.parentNode.href)){
				safari.self.tab.setContextMenuEventUserInfo(event, event.target.parentNode.href);
				return;
			}
		}
		if(testLink(event.target.src)){
			safari.self.tab.setContextMenuEventUserInfo(event, event.target.src);
		}
	}else if(event.target.nodeName === 'A'){
		var imgLinkTest = testParam(event.target.parentNode.href);
		var imgLink = loopObj(imgLinkTest) || 0;
		if(imgLink.length){
			safari.self.tab.setContextMenuEventUserInfo(event, imgLink);
			return;
		}else if(testLink(event.target.href)){
			safari.self.tab.setContextMenuEventUserInfo(event, event.target.href);
		}
	}
}

function testLink(link){
	if(link.indexOf('.jpg') > 0 || link.indexOf('.jpeg') > 0 || link.indexOf('.png') > 0 || link.indexOf('.tiff') > 0 || link.indexOf('.gif') > 0 || link.indexOf('.JPEG') > 0){
		return true;
	}
}


function testParam(link){
	var urlParams,
		match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = link;

    urlParams = {};
    while(match = search.exec(query)){
    	urlParams[decode(match[1])] = decode(match[2]); 
    }
    return urlParams;
}

function loopObj(obj){
	for(x in obj){
		if(testLink(obj[x])){
			return obj[x];
		}
	}
}


    
 